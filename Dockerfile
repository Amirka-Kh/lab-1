FROM eclipse-temurin:17-jdk-jammy as build-container
COPY . /app
RUN set -ex \
	&& cd /app \
	&& chmod +x mvnw \
	&& ./mvnw package


FROM eclipse-temurin:17-jdk-jammy
WORKDIR /app
COPY --from=build-container /app/target/main-0.0.1-SNAPSHOT.jar /app/app.jar
ENTRYPOINT [ "java", "-jar", "/app/app.jar" ]
